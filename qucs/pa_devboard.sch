<Qucs Schematic 0.0.20>
<Properties>
  <View=-50,-40,1710,3906,0.564474,0,240>
  <Grid=10,10,1>
  <DataSet=pa_devboard.dat>
  <DataDisplay=pa_devboard.dpl>
  <OpenDisplay=1>
  <Script=pa_devboard.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <GND *1 5 350 1220 0 0 0 0>
  <GND *2 5 600 1210 0 0 0 0>
  <GND *3 5 1120 1220 0 0 0 0>
  <GND *4 5 680 1210 0 0 0 0>
  <GND *5 5 810 1210 0 0 0 0>
  <Pac ANT1 1 1120 1150 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND *6 5 960 1220 0 0 0 0>
  <SPfile C220p1 1 680 1090 -26 -43 0 0 "/home/azisi/Documents/UPSat/qubik-rf/pa_devboard/qucs/s-parameters/GCM1885C2A221JA16_series.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <.SW SW1 0 -20 800 0 91 0 0 "SP1" 1 "lin" 1 "C_value" 1 "10 pF" 1 "20 pF" 1 "8" 1>
  <CAPQ CAPQ1 1 960 1160 17 -26 0 1 "20 pF" 1 "100" 1 "100 MHz" 0 "Linear" 0 "26.85" 0>
  <GND *7 5 510 1210 0 0 0 0>
  <.SW SW2 0 120 800 0 91 0 0 "SP1" 1 "lin" 1 "R_value" 1 "50 Ohm" 1 "500 Ohm" 1 "8" 1>
  <GND * 5 420 1220 0 0 0 0>
  <INDQ INDQ1 1 1400 1120 -26 -86 0 2 "1.2 nH" 1 "12" 1 "100 MHz" 0 "Linear" 0 "26.85" 0>
  <INDQ INDQ2 1 1300 1230 -26 17 0 0 "22.7 nH" 1 "77" 1 "900 MHz" 0 "Linear" 0 "26.85" 0>
  <INDQ INDQ3 1 1400 1230 -26 -86 0 2 "4.7 nH" 1 "25" 1 "250 MHz" 0 "Linear" 0 "26.85" 0>
  <INDQ INDQ5 1 1290 1160 -26 -86 0 2 "3.9 nH" 1 "40" 1 "100 MHz" 0 "Linear" 0 "20" 0>
  <SPfile C2 1 1290 1030 20 -26 1 1 "/home/azisi/Documents/UPSat/qubik-rf/pa_devboard/qucs/s-parameters/GRM1885C1H150JA01_series.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile C3 1 1290 950 20 -26 1 1 "/home/azisi/Documents/UPSat/qubik-rf/pa_devboard/qucs/s-parameters/GCM1885C1H180JA16_series.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <INDQ INDQ4 1 1400 1000 -26 -86 0 2 "5.6 nH" 1 "27" 1 "250 MHz" 0 "Linear" 0 "26.85" 0>
  <SPfile L5n6_6 1 510 1090 -26 -43 0 0 "/home/azisi/Documents/UPSat/qubik-rf/pa_devboard/qucs/s-parameters/LQG18HN5N6S00_series.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile L5n6_5 1 890 1090 -26 -43 0 0 "/home/azisi/Documents/UPSat/qubik-rf/pa_devboard/qucs/s-parameters/LQG18HN5N6S00_series.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <R R1 1 420 1160 15 -26 0 1 "R1" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "european" 0>
  <SPfile PA1 1 600 1090 -26 -43 0 0 "/home/azisi/Documents/UPSat/qubik-rf/pa_devboard/qucs/s-parameters/MMZ09332B_SP.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <Pac TX1 1 350 1150 -94 -26 1 1 "1" 1 "50 Ohm" 1 "-20 dBm" 0 "1 GHz" 0 "26.85" 0>
  <SPfile L22n_4 1 810 1140 -40 -26 0 1 "/home/azisi/Documents/UPSat/qubik-rf/pa_devboard/qucs/s-parameters/LQW18AN22NG00_series.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <.SP SP1 1 280 800 0 91 0 0 "lin" 1 "300 MHz" 1 "600 MHz" 1 "100" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <Eqn Eqn1 1 490 810 -28 15 0 0 "dBS11=dB(S[1,1])" 1 "dBS12=dB(S[1,2])" 1 "dBS22=dB(S[2,2])" 1 "dBS21=dB(S[2,1])" 1 "yes" 0>
  <Eqn SWR1 1 650 810 -28 15 0 0 "swr=rtoswr(S[1,1])" 1 "stand_wave_ratio=min(swr, 400e6:500e6)" 1 "yes" 0>
  <Eqn stability1 1 970 790 -28 15 0 0 "rollet=Rollet(S)" 1 "stab=max(StabFactor(S), 400e6:500e6)" 1 "GMAX=(1/(rollet+sqrt(rollet^2-1)))*(abs(dBS21)/abs(dBS12))" 1 "MSG=mag(dBS21)/mag(dBS12)" 1 "stab_factor=StabFactor(S)" 1 "mu= Mu(S)" 1 "mu2= Mu2(S)" 1 "yes" 0>
  <Eqn Zin1 1 650 910 -28 15 0 0 "Zin=(50)*(1+S[1,1])/(1-S[1,1])" 1 "Z_imag_in=imag(Zin)" 1 "Z_real_in=real(Zin)" 1 "yes" 0>
  <Eqn Zout1 1 10 1200 -28 15 0 0 "Zout=(50)*(1+S[2,2])/(1-S[2,2])" 1 "Z_imag_out=imag(Zout)" 1 "Z_real_out=real(Zout)" 1 "yes" 0>
  <.Opt Opt1 1 -20 1090 0 50 0 0 "Sim=SP1" 0 "DE=3|500|2|20|0.85|1|10|1e-6|10|100" 0 "Var=R1|yes|0|30|200|LIN_DOUBLE" 0 "Goal=stab_factor|GE|10" 0 "Goal=swr|LE|2" 0>
</Components>
<Wires>
  <350 1180 350 1220 "" 0 0 0 "">
  <600 1120 600 1210 "" 0 0 0 "">
  <1120 1180 1120 1220 "" 0 0 0 "">
  <630 1090 650 1090 "" 0 0 0 "">
  <680 1120 680 1210 "" 0 0 0 "">
  <710 1090 810 1090 "" 0 0 0 "">
  <1120 1090 1120 1120 "" 0 0 0 "">
  <810 1200 810 1210 "" 0 0 0 "">
  <810 1200 850 1200 "" 0 0 0 "">
  <960 1090 1120 1090 "" 0 0 0 "">
  <960 1090 960 1130 "" 0 0 0 "">
  <960 1190 960 1220 "" 0 0 0 "">
  <350 1090 350 1120 "" 0 0 0 "">
  <350 1090 420 1090 "" 0 0 0 "">
  <510 1120 510 1210 "" 0 0 0 "">
  <540 1090 570 1090 "" 0 0 0 "">
  <420 1090 480 1090 "" 0 0 0 "">
  <850 1200 890 1200 "" 0 0 0 "">
  <890 1120 890 1200 "" 0 0 0 "">
  <920 1090 960 1090 "" 0 0 0 "">
  <810 1090 860 1090 "" 0 0 0 "">
  <420 1090 420 1130 "" 0 0 0 "">
  <420 1190 420 1220 "" 0 0 0 "">
  <850 1140 850 1200 "" 0 0 0 "">
  <840 1140 850 1140 "" 0 0 0 "">
  <810 1090 810 1110 "" 0 0 0 "">
  <810 1170 810 1200 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 210 2746 1136 406 3 #c0c0c0 1 00 1 3e+08 2e+07 6e+08 1 -40 10 43.4402 1 -1 0.2 1 315 0 225 "" "" "" "">
	<"dBS21" #0000ff 0 3 0 0 0>
	  <Mkr 4.63636e+08 451 -512 3 0 0>
	  <Mkr 4.51515e+08 224 -511 3 0 0>
	  <Mkr 5e+08 656 -482 3 0 0>
	  <Mkr 4.21212e+08 5 -497 3 0 0>
	  <Mkr 4.33333e+08 500 -307 3 0 0>
	<"dBS11" #ff0000 0 3 0 0 0>
	<"dBS22" #ff00ff 0 3 0 0 0>
	<"dBS12" #00ff00 0 3 0 0 0>
  </Rect>
  <Rect 210 3886 1163 466 3 #c0c0c0 1 00 1 0 0.2 1 1 -0.1 0.5 1.1 1 -0.1 0.5 1.1 315 0 225 "" "" "" "">
	<"GMAX" #0000ff 0 3 0 0 0>
	  <Mkr 4.30303e+08 338 -415 3 0 0>
  </Rect>
  <Rect 210 3354 1156 487 3 #c0c0c0 1 00 1 0 0.2 1 1 -0.1 0.5 1.1 1 -0.1 0.5 1.1 315 0 225 "" "" "" "">
	<"MSG" #0000ff 0 3 0 0 0>
  </Rect>
  <Rect 210 2159 545 369 3 #c0c0c0 1 00 1 3e+08 2e+07 6e+08 0 -1 1 20 1 -1 0.2 1 315 0 225 "" "" "" "">
	<"rollet" #ff0000 0 3 0 0 0>
	  <Mkr 4.30303e+08 296 -248 3 0 0>
  </Rect>
  <Rect 830 2159 522 369 3 #c0c0c0 1 00 1 1e+08 1e+08 1e+09 1 0.481505 1 7.30431 1 -1 0.2 1 315 0 225 "" "" "" "">
	<"mu" #0000ff 0 3 0 0 0>
	<"mu2" #ff0000 0 3 0 0 0>
  </Rect>
  <Rect 200 1759 1143 449 3 #c0c0c0 1 00 1 3e+08 2e+07 6e+08 1 -20 10 41.5521 1 -1 0.5 1 315 0 225 "" "" "" "">
	<"Z_imag_in" #ff0000 0 3 0 0 0>
	  <Mkr 4.33333e+08 563 -207 3 0 0>
	<"Z_real_in" #ff00ff 0 3 0 0 0>
	  <Mkr 4.33333e+08 584 -428 3 0 0>
  </Rect>
  <Rect 200 294 1059 314 3 #c0c0c0 1 00 1 3e+08 2e+07 6e+08 1 1 0.5 3.08793 1 -1 0.5 1 315 0 225 "" "" "" "">
	<"swr" #0000ff 0 3 0 0 0>
	  <Mkr 4.36364e+08 552 -268 3 0 0>
  </Rect>
  <Rect 210 714 1052 365 3 #c0c0c0 1 00 1 0 0.2 1 1 -0.1 0.5 1.1 1 -0.1 0.5 1.1 315 0 225 "" "" "" "">
	<"Z_real_out" #0000ff 0 3 0 0 0>
	  <Mkr 4.33333e+08 167 -350 3 0 0>
	<"Z_imag_out" #ff0000 0 3 0 0 0>
	  <Mkr 4.33333e+08 217 -123 3 0 0>
  </Rect>
  <Tab 1240 840 350 90 3 #c0c0c0 1 00 1 0 1 1 1 0 1 1 1 0 1 1 315 0 225 "" "" "" "">
	<"stab" #0000ff 0 3 1 0 0>
	<"R1" #0000ff 0 3 1 0 0>
	<"stand_wave_ratio" #0000ff 0 3 1 0 0>
  </Tab>
</Diagrams>
<Paintings>
</Paintings>
